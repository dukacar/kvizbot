<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use GuzzleHttp\Client;
// use Illuminate\Support\Facades\DB;
// use Illuminate\Support\Facades\Cookie;
// use Illuminate\Support\Facades\Auth;
use Paragraf\ViberBot\Http\Http;
use Log;

use Paragraf\ViberBot\Bot;
use Paragraf\ViberBot\TextMessage;
use Paragraf\ViberBot\Event\ConversationStartedEvent;
use Paragraf\ViberBot\Event\MessageEvent;
use Paragraf\ViberBot\Model\ViberUser;

use App\Models\ConversationStartedModel;
use App\Models\SubscribeModel;
use App\Models\OnboardingModel;

use App\Models\OpenTriviaDb;

class ViberApiController extends Controller
{
    /**
     * Request
     *
     * @var Illuminate\Http\Request
     */
    private $request;

    /**
     * Messenger name
     *
     * @var string
     */
    private $messenger = 'viber';

    /**
     * Accept Viber API request
     *
     * @param  Illuminate\Http\Request  $request  Request object
     * @return void
     */
    public function index(Request $request)
    {
        //dd(env('OPENTDB_API_URL'));
        //dd(config('opentriviadb.opentdb_api_base_url'));
        $params = [
            'amount' => 2,
            'difficulty' => 'easy'
        ];
        $result = OpenTriviaDb::getQuestions($params);
        dd($result);
        //return response('OK', 200);
    }

    /**
     * Accept Viber API post request
     *
     * @param Request $request Request to viber webhook
     *
     * @return Response Response to Viber API
     */
    public function post(Request $request)
    {
        $this->request = $request;

        /*
        Log::info(
            'Incoming Viber API POST request: ' . PHP_EOL .
            'session: ' . PHP_EOL . print_r($request->session()->all(), true) . PHP_EOL .
            'request: ' . PHP_EOL . print_r($this->request->request, true) . PHP_EOL
        );
        */

        // Check if event type can be processed
        if (!empty($this->request->event)) {
            // Decide action based on event type
            switch ((string) $this->request->event) {
                case 'message':
                    return $this->processMessage();
                    break;
                case 'delivered':
                    return $this->processDelivered();
                    break;
                case 'seen':
                    return $this->processSeen();
                    break;
                case 'failed':
                    return $this->processFailed();
                    break;
                case 'subscribed':
                    return $this->processSubscribed();
                    break;
                case 'unsubscribed':
                    return $this->processUnsubscribed();
                    break;
                case 'conversation_started':
                    return $this->processConversationStarted();
                    break;
                default:
                    Log::info(
                        'Event can not be processed: ' . PHP_EOL . print_r($this->request->event, true) . PHP_EOL .
                        'Incoming Viber API POST request: ' . PHP_EOL .
                        'request: ' . PHP_EOL . print_r($this->request->request, true) . PHP_EOL
                    );
            }
        }
        else
        {
            Log::info(
                'Event is EMPTY: ' . PHP_EOL .
                'Incoming Viber API POST request: ' . PHP_EOL .
                'request: ' . PHP_EOL . print_r($this->request, true) . PHP_EOL
            );
        }
    }

    /**
     * Process message event
     *
     * @return Response Response to Viber API
     */
    private function processMessage() {
        //Log::info(
        //    'Incoming Viber API POST request: ' . PHP_EOL .
        //    'request: ' . PHP_EOL . print_r($this->request->request, true) . PHP_EOL
        //);

        // Ovde bi ga trebao obeleziti kao subscribed

        // Decide action based on event type
        switch ((string) $this->request->message['tracking_data']) {
            case 'onboarding':
                return $this->processOnboarding();
                break;
            case 'answer':
                return $this->processAnswer();
                break;
            default:
                Log::info(
                    'Message can not be processed: ' . $this->request->message['tracking_data'] . PHP_EOL .
                    'Incoming Viber API POST request: ' . PHP_EOL .
                    'request: ' . PHP_EOL . print_r($this->request->request, true) . PHP_EOL
                );
        }

        // If message is not the answer to the question (keyboard action), decide what to do, broadcast to the group?
        return response('OK', 200);
    }

    private function processDelivered() {
        return response('OK', 200);
    }

    private function processSeen() {
        return response('OK', 200);
    }

    private function processFailed() {
        return response('OK', 200);
    }

    private function processSubscribed() {
        $subscribe = new SubscribeModel($this->request, $this->messenger);
        $subscribe->setSubscribed();
        return response('OK', 200);
    }

    private function processUnsubscribed() {
        $subscribe = new SubscribeModel($this->request, $this->messenger);
        $subscribe->setUnsubscribed();
        return response('OK', 200);
    }

    /**
     * Process conversation_started event
     *
     * @return Response Response to Viber API
     */
    private function processConversationStarted() {

        //Log::info(
        //    'Incoming Viber API POST request: ' . PHP_EOL .
        //    'request: ' . PHP_EOL . print_r($this->request->request, true) . PHP_EOL
        //);

        $converstion_started = new ConversationStartedModel($this->request, $this->messenger);

        // Response with welcome message
        return response(
            (new Bot($this->request, $converstion_started->getWelcomeMessage()))
                ->on(new ConversationStartedEvent(
                            $this->request->timestamp,
                            $this->request->message_token,
                            new ViberUser($this->request->user['id'], $this->request->user['name']),
                            $this->request->type,
                            $this->request->context,
                            $this->request->subscribed
                         )
                )
                // ->replay($welcome_message->getMessage())
                ->sendWelcomeResponse()
        );
    }

    /**
     * Process onboarding message
     *
     * @return Response Response to Viber API
     */
    private function processOnboarding() {
        $onboarding = new OnboardingModel($this->request, $this->messenger);

        if (empty($onboarding->getMessage()))
        {
            return response('OK', 200);
        }

        (new Bot($this->request, $onboarding->getMessage()))
          ->on(new MessageEvent($this->request->timestamp, $this->request->message_token,
          new ViberUser($this->request->sender['id'], $this->request->sender['name']), $this->request->message))
          ->replay($onboarding->getMessageText())
          ->send();

        return response('OK', 200);
    }

    /**
     * Process answer message
     *
     * @return Response Response to Viber API
     */
    private function processAnswer() {
        return response('OK', 200);
    }
}
