<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;
use Paragraf\ViberBot\Http\Http;
use Log;

class SetWebhhokController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (getenv('APP_ENV') !== 'production')
        {
            return 'Allowed only at production! (' . getenv('APP_ENV') . ')';
        }

        $webhook_url = 'https://kvizbot.automatica.rs/viber_api_webhook';

        $result = Http::call('POST', 'set_webhook', [
            'url' => $webhook_url,
            'event_types' => config('viberbot.event_types'),
            'send_name'=> true,
            'send_photo'=> true,
        ]);

        if ($result->status === 0)
        {
            echo ('You initialize successfully your route! (' . $webhook_url . ')');
        }
        else
        {
            echo ('Something went wrong! (' . $webhook_url . ')<br/><br/>');
            echo ('status: \'' . $result->status . '\'<br/>');
            echo ('status_message: \'' . $result->status_message . '\'<br/>');
            echo ('chat_hostname: \'' . $result->chat_hostname . '\'<br/>');
        }

        Log::info('Setting Viber API webhook response: ' . PHP_EOL . print_r($result, true));

        return;
    }
}

