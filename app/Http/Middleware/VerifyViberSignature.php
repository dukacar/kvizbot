<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;
use Closure;
use Log;

class VerifyViberSignature {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {

        $signature = $request->header('X-Viber-Content-Signature');
        $payload = $request->getContent();

        $calculated_hmac = hash_hmac('sha256', $payload, config('viberbot.api-key'));

        if($signature != $calculated_hmac) {
            Log::info(
                'Signature WRONG!' . PHP_EOL .
                'X-Viber-Content-Signature: ' . $signature . PHP_EOL .
                'request' . PHP_EOL . print_r($request->request, true) . PHP_EOL
            );
            return response('OK', 200);
        }

        return $next($request);
    }

}
