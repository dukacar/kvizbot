<?php

namespace App\Models;

use Illuminate\Http\Request;

use App\Models\DbTables\ParticipantMessenger;

use App\Models\DbTables\Messenger;

class ParticipantModel extends MessengerModel
{
    /**
     * Participant id
     *
     * @var Int
     */
    protected $participant_id;

    /**
     * Instantiate conversation started
     *
     * @param  Illuminate\Http\Request  $request         Request object
     * @param  String                   $messenger_name  Messenger name
     * @return string
     */
    public function __construct(\Illuminate\Http\Request $request, String $messenger_name)
    {
        parent::__construct($request, $messenger_name);
        $this->participant_id = $this->getParticipantIdByUid();
    }

    /**
     * Get participant id by participiant's messenger user id
     *
     * @return Int or Null
     */
    Protected function getParticipantIdByUid()
    {
        return ParticipantMessenger::where('messenger_id', $this->messenger_id)
            ->where('messenger_uid', $this->request->user['id'])
            ->value('id');
    }
}
