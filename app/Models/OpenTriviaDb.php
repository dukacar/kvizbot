<?php

namespace App\Models;

//use App\Models\DbTables\ParticipantMessenger;
//use App\Models\DbTables\ParticipantGroup;
//use App\Models\DbTables\Round;

use Curl\Curl;

use Log;

class OpenTriviaDb
{
    /**
     * Request questions from Open trivia Database API
     *
     * @param  array  $params  API enpoint parameters
     * @return void
     */
    public static function getQuestions(array $params = []) {

        $curl = new Curl();

        $curl->get(env('OPENTDB_API_URL'), $params);

        if ($curl->error) {
            $response = [
                'error' => 1,
                'message' => $curl->errorMessage
            ];
            return json_encode($response);
            // return json_encode('Error: ' . $curl->errorCode . ': ' . $curl->errorMessage);
        }

        $response = $curl->response;

        if ($response->response_code !== 0 || empty($response->results))
        {
            // There are no questions retreived
            $response = [
                'error' => 2,
                'message' => 'Open Trivia Database is unavailable currently.'
            ];
            return json_encode($response);
            return json_encode('Error: 0: ' . $curl->errorMessage . "\n");
        }

        return $response->results;
    }

    public function buildQuery(array $filters = []) {
        $queryFilters = [];
        foreach($filters as $key => $value){
            $queryFilters[] = "$key:$value";
        }
        return join("%20", $queryFilters);
    }

    public function getListings(array $params = [], string $type = 'managed', string $filter = 'verified'){
        return $this->call("/listings/$type/$filter", $params);
    }

    public function getExtendedListings(array $params = [], string $type = 'managed', string $filter = 'verified'){
        return $this->getListings(array_merge($params, ['summary' => 'extended']), $type,  $filter);
    }

    public function getScoreReport(array $params = [], string $type = 'managed', string $filter = 'verified'){
        return $this->call("/listings/$type/$filter/reports/scores", $params);
    }

    public function getReachReport(array $params = [], string $type = 'managed', string $filter = 'verified'){
        return $this->call("/listings/$type/$filter/reports/reach", $params);
    }

    public function getAccuracyReport(array $params = [], string $type = 'managed', string $filter = 'verified'){
        return $this->call("/listings/$type/$filter/reports/accuracy", $params);
    }

    public function getReviews(string $status = 'all', string $source = 'all', int $page = 1){
        return $this->call("/reviews", [
            'source' => $source,
            'status' => $status,
            'page' => $page
        ]);
    }

    public function queryReviews(array $filters, string $status = 'all', string $source = 'all', int $page = 1){
        return $this->call("/reviews/search/" . $this->buildQuery($filters), [
            'source' => $source,
            'status' => $status,
            'page' => $page
        ]);
    }
}
