<?php

namespace App\Models;

class EventConstants
{
    const ONBOARDING = 'onboarding';
    const NEW_ROUND = 'new';
    const CONTINUE_ROUND = 'continue';
    const INSTRUCTIONS = 'instructions';
    const DIFFICULTY = 'difficulty';
    const QUESTIONS = 'questions'; // Questions count
    const START = 'start'; // Questions count
}
