<?php

namespace App\Models;

use Illuminate\Http\Request;

use App\Models\DbTables\ParticipantMessenger;
use App\Models\DbTables\Participant;
use App\Models\DbTables\ParticipantGroup;
use App\Models\DbTables\Round;

use Paragraf\ViberBot\Model\Button;
use Paragraf\ViberBot\Model\Keyboard;
use Paragraf\ViberBot\Messages\WelcomeMessage;

class ConversationStartedModel extends MessengerModel
{
    /**
     * Participant id
     *
     * @var Int
     */
    protected $participant_id;

    protected $welcome_buttons = [];
    protected static $welcome_buttons_type;
    protected $welcome_message;

    /**
     * Instantiate conversation started
     *
     * @param  Illuminate\Http\Request  $request         Request object
     * @param  String                   $messenger_name  Messenger name
     * @return string
     */
    public function __construct(\Illuminate\Http\Request $request, String $messenger_name)
    {
        parent::__construct($request, $messenger_name);

        $this->createParticipant();

        if (empty($this->request->context))
        {
            $this->qrCodeParticipant();
        }
        else
        {
            $this->referredParticipant();
        }
    }

    /**
     * Create participant
     *
     * @return void
     */
    public function createParticipant()
    {
        // Note: participant can trigger conversion_started event more than once
        // so he might already exist in the DB and might has ongoing participation in the round

        $this->participant_id = ParticipantMessenger::getParticipantIdByUid($this->messenger_id, $this->request->user['id']);

        if (empty($this->participant_id))
        {
            // Create participant record
            $participant = new Participant;
            $participant->name = $this->request->user['name'];
            $participant->save();
            $this->participant_id = $participant->id;

            // Create participant_messenger record
            $participant_messenger = new ParticipantMessenger;
            $participant_messenger->participant_id = $this->participant_id;
            $participant_messenger->messenger_id = $this->messenger_id;
            $participant_messenger->messenger_uid = $this->request->user['id'];
            $participant_messenger->avatar = $this->request->user['avatar'];
            $participant_messenger->language = $this->request->user['language'];
            $participant_messenger->county = $this->request->user['country'];
            // $participant_messenger->subscribed = $subscribed;
            // $participant_messenger->unsubscribed = $unsubscribed;
            $participant_messenger->api_version = $this->request->user['api_version'];
            $participant_messenger->save();
            $participant_messenger_id = $participant_messenger->id;
        }
    }

    /**
     * Process not referred paricipant, QR code is scanned
     *
     * @return string
     */
    public function qrCodeParticipant()
    {
        if ($this->isParticipantInActiveRound())
        {
            $this->welcome_message = 'Zdravo ' . $this->request->user['name'] . '!' .
                "\n" .
                'Drago mi je da te ponovo vidim.' .
                "\n\n" .
                'Već si uključen u postojeću kviz rundu sa grupom drugara.' .
                // Ovde se mogu dodati informacije o grupi
                "\n\n" .
                'Izaberi da li želiš da nastaviš takmičenje u postojećoj kviz rundi.' .
                "\n" .
                'Možeš da me obrišeš i neću te više uznemiravati (osim kao te neki prijatelj ne uključi u kviz rundu).';

            $this->welcome_buttons = self::getQrCodeContinueButtons();
        }
        else
        {
            $this->welcome_message = 'Zdravo ' . $this->request->user['name'] . '!' .
                "\n" .
                'Dobrodošao na Kviz Bot.' .
                "\n\n" .
                'Ako želiš da počneš klikni na dugme dole.' .
                "\n" .
                'Ako ne, obriši me, neću te više uznemiravati (osim kao te neki prijatelj ne uključi u kviz rundu).';

            $this->welcome_buttons = self::getQrCodeNewButtons();
        }

        $this->welcome_buttons[] = $this->getInstructionsButton();
    }

    public static function getQrCodeContinueButtons()
    {
        $buttons = [];

        // Continue with group
        $ActionBody = [
            'action' => 'continue',
            'step'   => EventConstants::CONTINUE_ROUND
        ];
        $welcome_button = new Button('reply', json_encode($ActionBody), 'Želim da nastavim.', 'regular');
        $welcome_button->setColumns(6);
        $welcome_button->setRows(1);
        $welcome_button->setBgColor('#9fd9f1');
        $buttons[] = $welcome_button;

        // Opt out from group
        $ActionBody = [
            'action' => 'continue',
            'step'   => EventConstants::NEW_ROUND
        ];
        $welcome_button = new Button('reply', json_encode($ActionBody), 'Isključi me iz grupe.', 'regular');
        $welcome_button->setColumns(6);
        $welcome_button->setRows(1);
        $welcome_button->setBgColor('#9fd9f1');
        $buttons[] = $welcome_button;

        self::$welcome_buttons_type = __FUNCTION__;

        return $buttons;
    }

    public static function getQrCodeNewButtons()
    {
        $buttons = [];

        $ActionBody = [
            'action' => 'continue',
            'step'   => EventConstants::NEW_ROUND
        ];
        $welcome_button = new Button('reply', json_encode($ActionBody), 'Želim da probam.', 'regular');
        $welcome_button->setColumns(6);
        $welcome_button->setRows(1);
        $welcome_button->setBgColor('#9fd9f1');
        $buttons[] = $welcome_button;

        self::$welcome_buttons_type = __FUNCTION__;

        return $buttons;
    }

    /**
     * Process referred participant, we need to join him to a group
     *
     * @return string
     */
    public function referredParticipant()
    {

    }

    /**
     * Check if user is participating in any ongoing round
     *
     * @return Bool
     */
    protected function isParticipantInActiveRound()
    {
        // Get groups where paricipant is active
        $participant_group_ids = ParticipantGroup::getParticipantGroupsActive($this->participant_id);
        if (empty($participant_group_ids))
        {
            // Participant doesn't belong to any group (thus no round started)
            return false;
        }

        // Check if there is not finished round for the group
        foreach ($participant_group_ids as $participant_group_id)
        {
            if (Round::hasActiveRoundsForGroup($participant_group_id))
            {
                return true;
            }
        }

        return true;
    }

    /**
     * Get instructions button
     *
     * @return Paragraf\ViberBot\Model\Button
     */
    protected function getInstructionsButton()
    {
        $ActionBody = [
            'action'  => EventConstants::INSTRUCTIONS,
            'buttons' => self::$welcome_buttons_type
        ];
        $welcome_button = new Button('reply', json_encode($ActionBody), 'Pokaži mi uputstvo.', 'regular');
        $welcome_button->setColumns(6);
        $welcome_button->setRows(1);
        $welcome_button->setBgColor('#9fd9f1');

        return $welcome_button;
    }

    /**
     * Get welcome message
     *
     * @return \Paragraf\ViberBot\Messages\WelcomeMessage
     */
    public function getWelcomeMessage()
    {
        $welcome_keyboard = new Keyboard($this->welcome_buttons);
        return new WelcomeMessage('text', $welcome_keyboard, $this->welcome_message);
    }
}
