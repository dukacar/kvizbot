<?php

namespace App\Models;

use Illuminate\Http\Request;
use App\Models\DbTables\ParticipantMessenger;

class SubscribeModel extends MessengerModel
{
    /**
     * Request
     *
     * @var \Illuminate\Http\Request
     */
    protected $request;

    /**
     * Messenger name
     *
     * @var String
     */
    protected $messenger_name = '';

    /**
     * Messenger id
     *
     * @var Int
     */
    protected $messenger_id;

    /**
     * Instantiate conversation started
     *
     * @param  Illuminate\Http\Request  $request         Request object
     * @param  String                   $messenger_name  Messenger name
     * @return string
     */
    public function __construct(\Illuminate\Http\Request $request, String $messenger_name)
    {
        parent::__construct($request, $messenger_name);
    }

    /**
     * Set participant subscribed
     *
     * @return void
     */
    public function setSubscribed()
    {
        $participant_messenger = ParticipantMessenger::where('messenger_id', $this->messenger_id)
            ->where('messenger_uid', $this->request->user['id'])
            ->first();

        $participant_messenger->subscribed = time();
        $participant_messenger->unsubscribed = 0;

        $participant_messenger->save();
    }

    /**
     * Set participant unsubscribed
     *
     * @return void
     */
    public function setUnsubscribed()
    {
        $participant_messenger = ParticipantMessenger::where('messenger_id', $this->messenger_id)
            ->where('messenger_uid', $this->request->user_id)
            ->first();

        $participant_messenger->subscribed = 0;
        $participant_messenger->unsubscribed = time();

        $participant_messenger->save();
    }
}
