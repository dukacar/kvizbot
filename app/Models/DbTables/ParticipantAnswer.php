<?php

namespace App\Models\DbTables;

use Illuminate\Database\Eloquent\Model;

class ParticipantAnswer extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'participant_answer';

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'U';

    /**
     * Delete paricipant answers by round question id
     *
     * @param  int  $round_question_id  Round question id
     * @return int
     */
    public static function deleteAnswerByQuestionId(int $round_question_id)
    {
        self::where('round_question_id', $round_question_id)->delete();
    }
}
