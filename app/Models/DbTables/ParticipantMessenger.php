<?php

namespace App\Models\DbTables;

use Illuminate\Database\Eloquent\Model;

class ParticipantMessenger extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'participant_messenger';

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'U';

    /**
     * Get participant id by messenger user id
     *
     * @param  int     $messenger_id   Messenger id
     * @param  string  $messenger_uid  Messenger user id
     * @return int
     */
    public static function getParticipantIdByUid(int $messenger_id, string $messenger_uid)
    {
        return self::where('messenger_id', $messenger_id)
            ->where('messenger_uid', $messenger_uid)
            ->value('id');
    }
}
