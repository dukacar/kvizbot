<?php

namespace App\Models\DbTables;

use Illuminate\Database\Eloquent\Model;

class Round extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'round';

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'U';

    /**
     * Check if there are active rounds for participant group
     *
     * @param  int  $participant_group_id  Paricipant group id
     * @return bool
     */
    public static function hasActiveRoundsForGroup(int $participant_group_id)
    {
        if (empty(self::where('participant_group_id', $participant_group_id)->where('status', '!=', 'finished')->value('id')))
        {
            return false;
        }

        return true;
    }

    /**
     * Delete all data for a group round
     *
     * @param  int  $participant_group_id  Paricipant group id
     * @return void
     */
    public static function deleteGroupRounds($participant_group_id)
    {
        // Find round id
        $round_id = self::where('participant_group_id', $participant_group_id)->value('id');

        if (!empty($round_id))
        {
            RoundQuestion::deleteRoundQuestionsByRoundId($round_id);
        }

        self::where('participant_group_id', $participant_group_id)->delete();
    }

    /**
     * Create a new round
     *
     * @param  int  $participant_group_id  Group id
     * @return int Round id
     */
    public function createRound(int $participant_group_id)
    {
        // Get max round number for a group
        $round_number = self::where('participant_group_id', $participant_group_id)->max('round_number');
        if (empty($round_number))
        {
            $round_number = 0;
        }
        $round_number++;

        $this->participant_group_id = $participant_group_id;
        $this->round_number = $round_number;
        $this->save();
        return $this->id;
    }

    /**
     * Update round difficulty
     *
     * @param  int     $round_id    Round id
     * @param  string  $difficulty  Difficulty (easy, medium, hard)
     * @return void
     */
    public static function updateDifficulty(int $round_id, string $difficulty)
    {
        self::where('id', $round_id)
            ->update(['difficulty' => $difficulty]);
    }

    /**
     * Update questions count
     *
     * @param  int     $round_id         Round id
     * @param  int     $questions_count  Questions count (5, 10, 20, 30, 50)
     * @return void
     */
    public static function updateQuestionsCount(int $round_id, int $questions_count)
    {
        self::where('id', $round_id)
            ->update(['questions_count' => $questions_count]);
    }
}
