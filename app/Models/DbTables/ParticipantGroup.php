<?php

namespace App\Models\DbTables;

use Illuminate\Database\Eloquent\Model;

class ParticipantGroup extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'participant_group';

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'U';

    /**
     * Get groups where participant is active
     *
     * @param  int  $participant_id  Participant id
     * @return array
     */
    public static function getParticipantGroupsActive(int $participant_id)
    {
        return self::where('participant_id', $participant_id)
            ->where('active', 1)
            ->pluck('id')
            ->toArray();
    }

    /**
     * Check if participant is single player in a group
     *
     * @param  int  $participant_group_id  Participant group id
     * @return bool
     */
    public static function isSingleGroup(int $participant_group_id)
    {
        if (self::where('id', $participant_group_id)->count() > 1)
        {
            return false;
        }

        return true;
    }

    /**
     * Deactivate participant in a participant group
     *
     * @param  int  $participant_id        Participant id
     * @param  int  $participant_group_id  Participant group id
     * @return bool
     */
    public static function deactivateParticipant(int $participant_id, int $participant_group_id)
    {
        self::where('id', $participant_group_id)
            ->update(['active' => 0]);
    }

    /**
     * Delete all cascading data by paricipant group id
     *
     * @param  int  $participant_group_id  Paricipant group id
     * @return bool
     */
    public static function deleteParticipantGroup(int $participant_group_id)
    {
        // Get group id
        $group_id = self::where('id', $participant_group_id)->value('group_id');

        if (!empty($group_id))
        {
            Group::deleteGroupById($group_id);
        }

        self::where('id', $participant_group_id)->delete();
    }

    /**
     * Create a new participant group
     *
     * @param  int  $participant_id  Participant id
     * @param  int  $group_id        Group id
     * @return int Participant gruop id
     */
    public function createParticipantGroup(int $participant_id, int $group_id)
    {
        $this->participant_id = $participant_id;
        $this->group_id = $group_id;
        $this->save();
        return $this->id;
    }
}
