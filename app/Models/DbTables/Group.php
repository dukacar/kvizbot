<?php

namespace App\Models\DbTables;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'group';

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'U';

    protected static $name;

    /**
     * Delete all cascading data by paricipant group id
     *
     * @param  int  $group_id  Group id
     * @return bool
     */
    public static function deleteGroupById(int $group_id)
    {
        self::where('id', $group_id)->delete();
    }

    /**
     * Create a new group
     *
     * @param  string  $group_name  Group name
     * @return int Group id
     */
    public function createGroup(string $group_name = '')
    {
        $this->name = $group_name;
        $this->save();
        return $this->id;
    }
}
