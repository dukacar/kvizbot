<?php

namespace App\Models;

use Illuminate\Http\Request;

use App\Models\DbTables\Messenger;

class MessengerModel
{
    /**
     * Request
     *
     * @var \Illuminate\Http\Request
     */
    protected $request;

    /**
     * Messenger name
     *
     * @var String
     */
    protected $messenger_name = '';

    /**
     * Messenger id
     *
     * @var Int
     */
    protected $messenger_id;

    /**
     * Instantiate conversation started
     *
     * @param  Illuminate\Http\Request  $request         Request object
     * @param  String                   $messenger_name  Messenger name
     * @return string
     */
    public function __construct(\Illuminate\Http\Request $request, String $messenger_name)
    {
        // TODO: Verify request depending on messenger
        $this->setRequest($request);
        $this->setMessangerName($messenger_name);
        $this->messenger_id = $this->getMessengerIdByName();
    }

    /**
     * Verify request data
     *
     * @param  Illuminate\Http\Request  $request  Request object
     * @var string
     */
    public function verifyRequestData()
    {
        return true;
    }

    /**
     * Set messenger
     *
     * @param  Illuminate\Http\Request  $request  Request object
     * @return void
     */
    public function setRequest(\Illuminate\Http\Request $request)
    {
        $this->request = $request;
    }

    /**
     * Set messenger
     *
     * @param  String  $messenger  Messenger
     * @return void
     */
    public function setMessangerName(String $messenger_name)
    {
        $this->messenger_name = $messenger_name;
    }

    /**
     * Get messenger
     *
     * @return string
     */
    public function getMessangerName()
    {
        return $this->messenger_name;
    }

    /**
     * Get messenger id by messanger name
     *
     * @return string
     */
    protected function getMessengerIdByName()
    {
        return Messenger::where('name', $this->messenger_name)->value('id');
    }
}
