<?php

namespace App\Models;

use Illuminate\Http\Request;

use App\Models\DbTables\ParticipantMessenger;
use App\Models\DbTables\ParticipantGroup;
use App\Models\DbTables\Round;
use App\Models\DbTables\Group;

use Paragraf\ViberBot\Model\Button;
use Paragraf\ViberBot\Model\Keyboard;
use Paragraf\ViberBot\Messages\Message;

use Log;

class OnboardingModel extends MessengerModel
{
    /**
     * Message text received from user
     *
     * @var Array
     */
    protected $message_text = [];

    /**
     * Participant id
     *
     * @var Int
     */
    protected $participant_id;

    /**
     * Group id
     *
     * @var Int
     */
    protected $group_id;

    /**
     * Participant group id
     *
     * @var Int
     */
    protected $participant_group_id;

    /**
     * Round id
     *
     * @var Int
     */
    protected $round_id;

    /**
     * Message buttons
     *
     * @var Array
     */
    protected $buttons = [];

    /**
     * Message text
     *
     * @var String
     */
    protected $message;

    /**
     * Instantiate conversation started
     *
     * @param  Illuminate\Http\Request  $request         Request object
     * @param  String                   $messenger_name  Messenger name
     * @return string
     */
    public function __construct(\Illuminate\Http\Request $request, String $messenger_name)
    {
        parent::__construct($request, $messenger_name);

        $this->message_text = json_decode((string) $this->request->message['text']);

        switch ($this->message_text->action) {
            case 'instructions':
                return $this->sendInstructions($this->message_text->buttons);
                break;
            case 'continue':
                $this->setPropertiesFromMessageText();
                return $this->continueOnboarding($this->message_text->step);
                break;
            default:
                Log::info(
                    'Message can not be processed: ' . $this->request->message['taxt'] . PHP_EOL .
                    'Incoming Viber API POST request: ' . PHP_EOL .
                    'request: ' . PHP_EOL . print_r($this->request->request, true) . PHP_EOL
                );
        }
    }

    /**
     * Compile instruction message
     *
     * @param  String  $buttons  ConversationStartedModel method name to get buttons
     * @return void
     */
    protected function sendInstructions(String $buttons)
    {
        $this->message = 'Uputsvo' . "\n" .
            '---------' . "\n" .
            "\n" .
            'Namenjen sam u svrhu zabave ali pomalo i za vežbanje tvog znanja.' . "\n" .
            "\n" .
            'Svakog dana, slaću ti po jedno pitanje dok se započeta runda kviza ne završi.' . "\n" .
            'Kada ti budem slao pitanje za sledeći dan, prethodno ću oceniti kao netačno ako na njega nisi odgovorio.' . "\n" .
            "\n" .
            'Za pitanja koristim Open Trivia Database u kojoj ima oko 3000 pitanja.' . "\n" .
            'Na žalost pitanja i odgovori su na engleskom.' . "\n" .
            "\n" .
            'Single player:' . "\n" .
            'Imaćeš priliku da započneš rundu, izabereš broj pitanja i težinu.' . "\n" .
            'Učestvuješ sam i na kraju runde prikazaću ti rezultat.' . "\n" .
            'Takođe ćeš na kraju runde moći da započneš novu.' . "\n" .
            "\n" .
            'Multi player:' . "\n" .
            'Imaćeš priliku da u rundu koju započneš pozoveš drugare da ti se priključe.' . "\n" .
            'Svaki od učesnika će, dnevno, dobiti isto pitanje.' . "\n" .
            'Na kraju runde prikazaću svima rang listu kao i svakome svoj rezultat.' . "\n" .
            'Rangiraću odgovore po tačnosti i brzini odgovora (period od kada vidiš poruku do kada odgovoriš).' . "\n" .
            'Ako se neko od učesnika isključi iz runde, isključiću ga iz rang liste.' . "\n" .
            "\n" .
            'Svaki igrač može da učestvuje samo u jednoj rundi.' . "\n" .
            'Ako igrač dobije poziv od drugara da se priključi rundi a već učestvuje u jednoj, imaće priliku da odbije ili da prekine tekuću i priključi se novoj.' . "\n" .
            'Ako želiš da prekineš tekuću rundu i započneš novu, potrebno je da skeniraš QR kod na kvizbot.automatica.rs.' . "\n" .
            "\n" .
            'U svakom trenutku možeš me obrisati, isključiću te iz kviza i neću te više uznemiravati.';

        // We need to send keyboard send in welcome message
        $this->buttons = call_user_func('App\Models\ConversationStartedModel::' . $buttons);
    }

    /**
     * Set class properties from message text received from user input
     *
     * @return void
     */
    protected function setPropertiesFromMessageText()
    {
        // Set participant id
        $this->participant_id = ParticipantMessenger::getParticipantIdByUid(
            $this->messenger_id,
            $this->request->sender['id']
        );

        // Set round id
        if (!empty($this->message_text->round_id))
        {
            $this->round_id = $this->message_text->round_id;
        }
    }

    /**
     * Compile instruction message
     *
     * @return void
     */
    public function continueOnboarding()
    {
        switch ($this->message_text->step) {
            case EventConstants::NEW_ROUND:
                $this->createNewRound();
                return $this->askDifficulty();
                break;
            case EventConstants::CONTINUE_ROUND:
                return $this->sendContinueRound(); // !!!!!!!!!!!!!!!!!!!!!
                break;
            case EventConstants::DIFFICULTY:
                $this->saveDifficulty();
                return $this->askQuestionsCount();
                break;
            case EventConstants::QUESTIONS:
                $this->saveQuestionsCount();
                $this->startRound();
                break;
            case EventConstants::START:
                die();
                //$this->saveQuestionsCount();
                //$this->startRound();
                break;
            default:
                Log::info(
                    'Onboarding can not be processed: ' . $this->request->message['text'] . PHP_EOL .
                    'Incoming Viber API POST request: ' . PHP_EOL .
                    'request: ' . PHP_EOL . print_r($this->request->request, true) . PHP_EOL
                );
        }
    }

    /**
     * Delete existing paricipants round(s) with cascading data and create new
     *
     * @return void
     */
    protected function createNewRound()
    {
        // pokupiti sve grupe gde je participant active
        $participant_group_ids = ParticipantGroup::getParticipantGroupsActive($this->participant_id);
        Log::info(
            'Participant Groups (' . $this->participant_id . '): ' . PHP_EOL .
            'session: ' . PHP_EOL . print_r($participant_group_ids, true) . PHP_EOL .
            'request: ' . PHP_EOL . print_r($this->request->request, true) . PHP_EOL
        );

        foreach ($participant_group_ids as $participant_group_id)
        {
            if (ParticipantGroup::isSingleGroup($participant_group_id))
            {
                Round::deleteGroupRounds($participant_group_id);
                ParticipantGroup::deleteParticipantGroup($participant_group_id);
            }
            else
            {
                // Deactivate participant in group
                ParticipantGroup::deactivateParticipant($this->participant_id, $participant_group_id);
            }
        }

        // Create new group
        $group = new Group;
        $this->group_id = $group->createGroup($this->request->sender['name']);
        // Create new participant_group
        $participant_group = new ParticipantGroup;
        $this->participant_group_id = $participant_group->createParticipantGroup($this->participant_id, $this->group_id);
        // Create a new round
        $round = new Round;
        $this->round_id = $round->createRound($this->participant_group_id);
    }

    /**
     * Compile difficulty message
     *
     * @return void
     */
    protected function askDifficulty()
    {
        $this->message = 'Izaberi tezinu za novu rundu.';

        $buttons = [];

        // Easy
        $ActionBody = [
            'action'     => 'continue',
            'step'       => EventConstants::DIFFICULTY,
            'difficulty' => 'easy',
            'round_id'   => $this->round_id
        ];
        $button = new Button('reply', json_encode($ActionBody), 'Lako', 'regular');
        $button->setColumns(6);
        $button->setRows(1);
        $button->setBgColor('#9fd9f1');
        $this->buttons[] = $button;

        // Medium
        $ActionBody = [
            'action'     => 'continue',
            'step'       => EventConstants::DIFFICULTY,
            'difficulty' => 'medium',
            'round_id'   => $this->round_id
        ];
        $button = new Button('reply', json_encode($ActionBody), 'Osrednje', 'regular');
        $button->setColumns(6);
        $button->setRows(1);
        $button->setBgColor('#9fd9f1');
        $this->buttons[] = $button;

        // Hard
        $ActionBody = [
            'action'     => 'continue',
            'step'       => EventConstants::DIFFICULTY,
            'difficulty' => 'hard',
            'round_id'   => $this->round_id
        ];
        $button = new Button('reply', json_encode($ActionBody), 'Teško', 'regular');
        $button->setColumns(6);
        $button->setRows(1);
        $button->setBgColor('#9fd9f1');
        $this->buttons[] = $button;
    }

    protected function sendContinueRound()
    {

    }

    /**
     * Save difficulty
     *
     * @return void
     */
    protected function saveDifficulty()
    {
        Round::updateDifficulty($this->message_text->round_id, $this->message_text->difficulty);
    }

    /**
     * Compile questions count message
     *
     * @return void
     */
    protected function askQuestionsCount()
    {
        $this->message = 'Izaberi broj pitanja za novu rundu.';

        $buttons = [];

        // 5
        $ActionBody = [
            'action'   => 'continue',
            'step'     => EventConstants::QUESTIONS,
            'count'    => 5,
            'round_id' => $this->round_id
        ];
        $button = new Button('reply', json_encode($ActionBody), 5, 'regular');
        $button->setColumns(1);
        $button->setRows(1);
        $button->setBgColor('#9fd9f1');
        $this->buttons[] = $button;

        // 10 - 50
        for ($x = 10; $x <= 50; $x+=10) {
            $ActionBody = [
                'action'   => 'continue',
                'step'     => EventConstants::QUESTIONS,
                'count'    => $x,
                'round_id' => $this->round_id
            ];
            $button = new Button('reply', json_encode($ActionBody), $x, 'regular');
            $button->setColumns(1);
            $button->setRows(1);
            $button->setBgColor('#9fd9f1');
            $this->buttons[] = $button;
        }
    }

    /**
     * Save questions count
     *
     * @return void
     */
    protected function saveQuestionsCount()
    {
        Round::updateQuestionsCount($this->message_text->round_id, $this->message_text->count);
    }

    /**
     * Compile message for finishing creating round
     * Invite friends oor continue as single player
     *
     * @return void
     */
    protected function startRound()
    {
        $this->message = 'Pozovi prijatelje u rundu ili nastavi sam.';

        $buttons = [];

        // Invite contacts
        $ActionBody = [
            'action'     => 'continue',
            'step'       => EventConstants::START,
            'type'       => 'multi',
            'round_id'   => $this->round_id
        ];
        $forward_url = 'viber://forward?text=viber://pa?chatURI=testUri%26context=' . $this->request->sender['id'];
        $button = new Button('open-url', $forward_url, 'Pozovi prijatelje...', 'regular');
        $button->setColumns(6);
        $button->setRows(1);
        $button->setSilent(true);
        $button->setBgColor('#9fd9f1');
        $button->setActionButton('forward');
        $button->setActionPredefinedURL($forward_url);
        $this->buttons[] = $button;

        // Single
        $ActionBody = [
            'action'     => 'continue',
            'step'       => EventConstants::START,
            'type'       => 'single',
            'round_id'   => $this->round_id
        ];
        $button = new Button('reply', json_encode($ActionBody), 'Igraću sam.', 'regular');
        $button->setColumns(6);
        $button->setRows(1);
        $button->setBgColor('#9fd9f1');
        $this->buttons[] = $button;
    }

    /**
     * Get message
     *
     * @return \Paragraf\ViberBot\Messages\Message
     */
    public function getMessage()
    {
        $keyboard = new Keyboard($this->buttons);
        $message = new Message('text', $keyboard);
        $message->setTrackingData(EventConstants::ONBOARDING);
        return $message;
    }

    /**
     * Get message text
     *
     * @return String
     */
    public function getMessageText()
    {
        return $this->message;
    }
}
