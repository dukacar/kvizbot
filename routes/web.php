<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//use Log;

// $json = '{"response_code":0,"results":[{"category":"General Knowledge","type":"multiple","difficulty":"easy","question":"How tall is the Burj Khalifa?","correct_answer":"2,722 ft","incorrect_answers":["2,717 ft","2,546 ft","3,024 ft"]},{"category":"General Knowledge","type":"multiple","difficulty":"easy","question":"What is the closest planet to our solar system&#039;s sun?","correct_answer":"Mercury","incorrect_answers":["Mars","Jupiter","Earth"]}]}';
// echo "<pre>";
// print_r(json_decode($json));
// echo "</pre>";

Route::get('/', function () {
    return view('welcome');
});

Route::get('/viber_api_webhook', 'ViberApiController@index');
Route::post('/viber_api_webhook', 'ViberApiController@post')->middleware('verifyViberSignature');

// Route::get('/clear-cache', 'ClearCacheController@index')->middleware('can:register_user');
Route::get('/clear-cache', 'ClearCacheController@index');

// Route::get('/db-migrate', 'DbMigrateController@index')->middleware('can:register_user');
Route::get('/db-migrate', 'DbMigrateController@index');

Route::get('/set-webhook', 'SetWebhhokController@index');
